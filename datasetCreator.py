import cv2
import numpy as np
faceDetectFrontal = cv2.CascadeClassifier('lbpcascade_frontalface_improved.xml')
#faceDetectProfile = cv2.CascadeClassifier('lbpcascade_profileface.xml');
#rightEyeBlink = cv2.CascadeClassifier('haarcascade_righteye_2splits.xml');
#leftEyeBlink = cv2.CascadeClassifier('haarcascade_lefteye_2splits.xml');
#smileDetect = cv2.CascadeClassifier('haarcascade_smile.xml');
cam = cv2.VideoCapture(1);

id=raw_input('enter user id: ')
username=raw_input("enter username")
file=open("users.txt","a")
file.write(id+" | "+username+'\n')
file.close()
sampleNum=0;
while(True):
    ret, img = cam.read();
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    facesFrontal = faceDetectFrontal.detectMultiScale(gray,1.1,5);
    for(x,y,w,h) in facesFrontal:
        sampleNum=sampleNum+1;
        cv2.imwrite("dataSet/User."+str(id)+"."+str(sampleNum)+".jpg",gray[y:y+h,x:x+w])
        cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
        cv2.waitKey(100);
    cv2.imshow("Face", img);
    cv2.waitKey(1);
    if(sampleNum>20):
        break;
cam.release()
cv2.destroyAllWindows()
