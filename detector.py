import cv2
import numpy as np

faceDetectFrontal = cv2.CascadeClassifier('lbpcascade_frontalface.xml')
#faceDetectProfile = cv2.CascadeClassifier('lbpcascade_profileface.xml');
#rightEyeBlink = cv2.CascadeClassifier('haarcascade_righteye_2splits.xml');
#leftEyeBlink = cv2.CascadeClassifier('haarcascade_lefteye_2splits.xml');
#smileDetect = cv2.CascadeClassifier('haarcascade_smile.xml');
video = cv2.VideoCapture(1);

recognizer=cv2.face.LBPHFaceRecognizer_create();
recognizer.read("recognizer\\trainingData.yml")
id=""
lines=""
file=open("users.txt","r")
lines=file.readlines()
font=cv2.FONT_HERSHEY_COMPLEX_SMALL
fontscale=1
fontcolor=(0,255,0)
while(True):
    ret, img = video.read();
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    facesFrontal = faceDetectFrontal.detectMultiScale(gray,1.1,5);
    for(x,y,w,h) in facesFrontal:
        cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
        id, conf=recognizer.predict(gray[y:y+h,x:x+w])
        print recognizer.predict(gray[y:y+h,x:x+w])
        userID, username=lines[id-1].split(" | ")
        print username
        cv2.putText(img,str(username),(x,y+h),font,fontscale,fontcolor);
        cv2.putText(img,str(conf),(x,y+h+20),font,fontscale,fontcolor);
    img=cv2.resize(img, (1024, 768))
    cv2.imshow("Face", img);
    if(cv2.waitKey(1)==ord('q')):
        break;
video.release()
cv2.destroyAllWindows()
